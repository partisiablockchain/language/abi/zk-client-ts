# Typescript ZK-client

Enables the generation of rpc for Off/On-chain ZK inputs, and reconstruction of a secret variable.

See examples for how to use the library.
* [Zk on chain input](src/test/example/ZkOnChainInput.ts) for how to send a ZK on chain input to a ZK contract on the testnet.
* [Zk off chain input](src/test/example/ZkOffChainInput.ts) for how to send a ZK off chain input to a ZK contract on the testnet.
* [Zk input reconstruction](src/test/example/ZkInputReconstruction.ts) for how to reconstruct a secret variable of a ZK contract on the testnet.

The examples can be run through node (requires node version >= 18), eg.

```bash
npx ts-node src/test/example/ZkInputReconstruction.ts
```
