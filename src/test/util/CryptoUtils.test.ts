/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { ec as Elliptic } from "elliptic";
import { CryptoUtils } from "../../main";

test("shared key #1", () => {
  const privateKey1 = "01";
  const privateKey2 = "02";

  // private 1 with public 2
  expect(
    CryptoUtils.createSharedKey(
      CryptoUtils.privateKeyToKeypair(privateKey1),
      CryptoUtils.privateKeyToPublicKey(privateKey2)
    ).toString("hex")
  ).toEqual("e6c08dd9cb0f0a0b6db0239166fff1eb032a54cdcc64b7ab563e781b0d92726a");

  // private 2 with public 1
  expect(
    CryptoUtils.createSharedKey(
      CryptoUtils.privateKeyToKeypair(privateKey2),
      CryptoUtils.privateKeyToPublicKey(privateKey1)
    ).toString("hex")
  ).toEqual("e6c08dd9cb0f0a0b6db0239166fff1eb032a54cdcc64b7ab563e781b0d92726a");
});

test("shared key #2", () => {
  const privateKey1 = "ce0000000000000000000000000000000000000001";
  const privateKey2 = "ce000000000000000000000000000000000000fefe";

  // private 1 with public 2
  expect(
    CryptoUtils.createSharedKey(
      CryptoUtils.privateKeyToKeypair(privateKey1),
      CryptoUtils.privateKeyToPublicKey(privateKey2)
    ).toString("hex")
  ).toEqual("45224d8a2ba2b82109f58bca7a01f38549cdcb207586c6e52320da54fd5ff76c");
  // private 2 with public 1
  expect(
    CryptoUtils.createSharedKey(
      CryptoUtils.privateKeyToKeypair(privateKey2),
      CryptoUtils.privateKeyToPublicKey(privateKey1)
    ).toString("hex")
  ).toEqual("45224d8a2ba2b82109f58bca7a01f38549cdcb207586c6e52320da54fd5ff76c");
});

test("AES encrypt #1", () => {
  const privateKey1 = "01";
  const privateKey2 = "02";

  const aes = CryptoUtils.createAesForParty(
    CryptoUtils.privateKeyToKeypair(privateKey1),
    CryptoUtils.privateKeyToPublicKey(privateKey2)
  );

  const value = Buffer.alloc(8, 0x56);
  const encryptedValue = Buffer.concat([aes.update(value), aes.final()]);

  expect(encryptedValue.toString("hex")).toEqual("014e83a6fb5003596830d3e315a91705");
});

test("AES encrypt #2", () => {
  const privateKey1 = "000000000000000000000000000000000000000001";
  const privateKey2 = "00000000000000000000000000000000000000fefe";

  {
    const aes = CryptoUtils.createAesForParty(
      CryptoUtils.privateKeyToKeypair(privateKey1),
      CryptoUtils.privateKeyToPublicKey(privateKey2)
    );

    const value = Buffer.alloc(8, 0x56);
    const encryptedValue = Buffer.concat([aes.update(value), aes.final()]);

    expect(encryptedValue.toString("hex")).toEqual("1af6592453ce9e63388f644cad0b08b8");
  }

  {
    const aes = CryptoUtils.createAesForParty(
      CryptoUtils.privateKeyToKeypair(privateKey1),
      CryptoUtils.privateKeyToPublicKey(privateKey2)
    );

    const value = Buffer.from(
      "0035e97a5e078a5a0f28ec96d547bfee9ace803ac0020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020268696969",
      "hex"
    );
    const encryptedValue = Buffer.concat([aes.update(value), aes.final()]);

    expect(encryptedValue.toString("hex")).toEqual(
      "9d70cc87a2ea67fd2c0418d7a2a63d7fc159d084b1a22ce6caaea8c0ec9180c79df1a03d2112856dd9562797ef00f6d87c597a9b926a3ca7256b5ea9dd5e8600ab5a4c2c1770b0479c453ed0e6252efceb067a8735f6472000ed146ca34a1190"
    );
  }
});

test("key to address", () => {
  const privateKey = "1";
  const publicKey = CryptoUtils.privateKeyToPublicKey(privateKey);
  expect(CryptoUtils.privateKeyToAccountAddress(privateKey)).toEqual(
    "0035e97a5e078a5a0f28ec96d547bfee9ace803ac0"
  );
  expect(CryptoUtils.privateKeyToAccountAddress(privateKey)).toEqual(
    CryptoUtils.publicKeyToAccountAddress(publicKey)
  );
});

test("shared key without prepending 0 byte", () => {
  const publicKey = CryptoUtils.privateKeyToKeypair("1").getPublic().encode("array", true);
  const publicKeyBuffer = Buffer.from(publicKey);
  const keyPair = CryptoUtils.privateKeyToKeypair(
    "dab92203ffd335ed3ef47f9bf44b5fe07af74e424c810647b41e8eedc4abe68c"
  );
  const sharedKey = CryptoUtils.createSharedKey(keyPair, publicKeyBuffer);
  expect(sharedKey.toString("hex")).toEqual(
    "3923aa6c75c750304d283fda1a4a913feafe7816c2afbc6643102557f740c944"
  );
});

test("generate key", () => {
  const privateKey = CryptoUtils.generateKeyPair();

  expect(privateKey.getPublic).not.toBeUndefined();
  expect(privateKey.sign).not.toBeUndefined();
});

test("signature to buffer with 31 byte s value test", () => {
  const privateKey = "07f18365046dd1445d72f796c75748dbfa2b69d5cd2db295dd1b6b42d85ca713";
  const hash = "9a16f2c40583ca831df266e68339f719c2ff304e5c455611f6d940d968d64c31";
  const buffer = Buffer.from(hash, "hex");
  const expectedSignature =
    "00" +
    "ed68c4457d0c187c7218699b8dddf161f38a40099e3b874b684b8c377d3091e3" +
    "00e1aa0f050cfd9ed08d95468b3b915c6a030c84aee2020c84375c60c8f12d2d";

  const signature = CryptoUtils.privateKeyToKeypair(privateKey).sign(buffer);
  const serializedSignature = CryptoUtils.signatureToBuffer(signature);
  expect(serializedSignature.toString("hex")).toEqual(expectedSignature);
});

test("signature without recovery param", () => {
  const signature = { recoveryParam: null } as Elliptic.Signature;
  expect(() => CryptoUtils.signatureToBuffer(signature)).toThrowError("Recovery parameter is null");
});

test("hash buffer", () => {
  const buffer = CryptoUtils.hashBuffer(Buffer.alloc(8));
  expect(buffer.toString("hex")).toBe(
    "af5570f5a1810b7af78caf4bc70a660f0df51e42baf91d4de5b2328de0e83dfc"
  );
});
