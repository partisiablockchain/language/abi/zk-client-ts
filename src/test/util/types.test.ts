/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { BlockchainAddress } from "../../main";
import { BlockchainPublicKey } from "../../main";

test("blockchain address", () => {
  expect(() => BlockchainAddress.fromBuffer(Buffer.alloc(0))).toThrowError(
    "BlockchainAddress expects exactly 21 bytes, but found 0"
  );
  expect(() => BlockchainAddress.fromBuffer(Buffer.alloc(22))).toThrowError(
    "BlockchainAddress expects exactly 21 bytes, but found 22"
  );
  const val1 = Buffer.alloc(21);
  const val2 = "00".repeat(21);
  const ba = BlockchainAddress.fromBuffer(val1);
  expect(ba).toEqual(BlockchainAddress.fromString(val2));
  expect(ba.asBuffer()).toEqual(val1);
  expect(ba.asString()).toEqual(val2);
});

test("blockchain public key", () => {
  expect(() => BlockchainPublicKey.fromBuffer(Buffer.alloc(0))).toThrowError(
    "BlockchainPublicKey expects exactly 33 bytes, but found 0"
  );
  expect(() => BlockchainPublicKey.fromBuffer(Buffer.alloc(34))).toThrowError(
    "BlockchainPublicKey expects exactly 33 bytes, but found 34"
  );
  const val1 = Buffer.alloc(33);
  const val2 = "00".repeat(33);
  const pk = BlockchainPublicKey.fromBuffer(val1);
  expect(pk).toEqual(BlockchainPublicKey.fromString(val2));
  expect(pk.asBuffer()).toEqual(val1);
  expect(pk.asString()).toEqual(val2);
});
