/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { BinaryExtensionFieldElement } from "../../main";
import { Polynomial } from "../../main";

describe("Polynomial tests", () => {
  test("evaluate", () => {
    const coefficients = [
      BinaryExtensionFieldElement.createElement(2),
      BinaryExtensionFieldElement.createElement(4),
      BinaryExtensionFieldElement.createElement(8),
    ];

    const polynomial = Polynomial.create(coefficients, BinaryExtensionFieldElement.ZERO);
    const evaluate = polynomial.evaluate(BinaryExtensionFieldElement.createElement(4));
    expect(evaluate.value).toBe(10);
  });

  test("filter high zeros", () => {
    const coefficients = [
      BinaryExtensionFieldElement.createElement(2),
      BinaryExtensionFieldElement.createElement(4),
      BinaryExtensionFieldElement.createElement(0),
    ];
    const polynomial = BinaryExtensionFieldElement.createPoly(coefficients);
    expect(polynomial.degree()).toEqual(1);
    const filteredCoefficients = polynomial.getCoefficients();
    expect(filteredCoefficients.length).toEqual(2);
    expect(filteredCoefficients[0]).toEqual(coefficients[0]);
    expect(filteredCoefficients[1]).toEqual(coefficients[1]);
  });

  test("zero polynomial", () => {
    const coefficients = [
      BinaryExtensionFieldElement.createElement(0),
      BinaryExtensionFieldElement.createElement(0),
      BinaryExtensionFieldElement.createElement(0),
    ];

    const polynomial = BinaryExtensionFieldElement.createPoly(coefficients);
    expect(polynomial.degree()).toEqual(0);
    expect(polynomial.getCoefficients()[0]).toEqual(BinaryExtensionFieldElement.ZERO);
  });
});
