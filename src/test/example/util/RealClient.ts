/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { BigEndianByteOutput } from "@secata-public/bitmanipulation-ts";
import { BinarySecretShares, BlindedShares, BlockchainAddress, CryptoUtils } from "../../../main";
import { Engine, ZkContract } from "./Client";
import { postRequest, putRequest } from "./Api";

/** A client for interacting with ZK engines on a Partisia blockchain (PBC). */
export class RealClient {
  private static readonly VARIABLE_HEADER: number[] = [
    "s",
    "h",
    "a",
    "m",
    "i",
    "r",
    "v",
    "a",
    "r",
    "i",
    "a",
    "b",
    "l",
    "e",
  ].map((s) => s.charCodeAt(0));

  private readonly engines: Engine[];

  private constructor(engines: Engine[]) {
    this.engines = engines;
  }

  /**
   * Creates a RealClient based on the engines found in the state of a ZK contract.
   *
   * @param zkContract to get the engines from.
   * @return the RealClient.
   */
  public static create(zkContract: ZkContract): RealClient {
    return new RealClient(zkContract.serializedContract.engines.engines);
  }

  /**
   * Gets the shares of a secret contract variable belonging to the private key.
   *
   * @param contractAddress the contract address that contains the secret variable.
   * @param privateKey the owner of the secret variable.
   * @param variableId the id of the variable.
   * @return a promise containing the secret shares.
   */
  public getSharesFromEngines(
    contractAddress: BlockchainAddress,
    privateKey: string,
    variableId: number
  ): Promise<BinarySecretShares> {
    return Promise.all(
      this.engines.map((engine) =>
        RealClient.getSharesFromEngine(engine, contractAddress, privateKey, variableId)
      )
    ).then((shares) => BinarySecretShares.read(shares));
  }

  private static getSharesFromEngine(
    engine: Engine,
    contractAddress: BlockchainAddress,
    privateKey: string,
    variableId: number
  ): Promise<Buffer> {
    const messageToBeSigned = BigEndianByteOutput.serialize((out) => {
      out.writeBytes(Buffer.from(RealClient.VARIABLE_HEADER));
      out.writeBytes(Buffer.from(engine.identity, "hex"));
      out.writeBytes(contractAddress.asBuffer());
      out.writeI32(variableId);
    });

    const keyPair = CryptoUtils.privateKeyToKeypair(privateKey);
    const signedMessage = CryptoUtils.signatureToBuffer(
      keyPair.sign(CryptoUtils.hashBuffer(messageToBeSigned))
    );
    const url = `${
      engine.restInterface
    }/real/binary/output/variable/${contractAddress.asString()}/${variableId}`;

    return postRequest(url, { signature: signedMessage.toString("base64") })
      .then((result) => result as LargeByteArrayI)
      .then((response) => Buffer.from(response.data, "base64"));
  }

  /**
   * Sends a secret share to the engines off chain.
   *
   * @param contract the contract of the secret variable.
   * @param account the owner of the secret variable.
   * @param txIdentifier the transaction hash of the hash of the secret shares.
   * @param blindedShares the blinded secret shares.
   * @return a promise containing whether it succeeded or not.
   */
  public sendSharesToNodes(
    contract: BlockchainAddress,
    account: BlockchainAddress,
    txIdentifier: Buffer,
    blindedShares: BlindedShares
  ): Promise<boolean> {
    return Promise.all(
      this.engines.map((engine, i) =>
        RealClient.sendShareToEngine(
          engine,
          contract,
          account,
          txIdentifier,
          blindedShares.shares[i]
        )
      )
    ).then((list: boolean[]) => {
      return list.every((b) => b);
    });
  }

  private static sendShareToEngine(
    engine: Engine,
    contract: BlockchainAddress,
    account: BlockchainAddress,
    txIdentifier: Buffer,
    blindedShare: Buffer
  ): Promise<boolean> {
    const url = `${
      engine.restInterface
    }/real/binary/input/${contract.asString()}/${account.asString()}/${txIdentifier.toString(
      "hex"
    )}`;
    return putRequest(url, { data: blindedShare.toString("base64") });
  }
}

interface LargeByteArrayI {
  // base64 string
  readonly data: string;
}
