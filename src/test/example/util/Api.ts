/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

const postHeaders: HeadersInit = {
  Accept: "application/json, text/plain, */*",
  "Content-Type": "application/json",
};

const getHeaders: HeadersInit = {
  Accept: "application/json, text/plain, */*",
};

export type RequestType = "GET" | "PUT" | "POST";

function buildOptions<T>(method: RequestType, headers: HeadersInit, entityBytes: T) {
  const result: RequestInit = { method, headers, body: null };

  if (entityBytes != null) {
    result.body = JSON.stringify(entityBytes);
  }
  return result;
}

/**
 * Make a http get-request.
 *
 * @param url the url to request.
 * @return a promise containing the result of the get request.
 */
export function getRequest<R>(url: string): Promise<R | undefined> {
  const options = buildOptions("GET", getHeaders, null);
  return handleFetch(fetch(url, options));
}

/**
 * Make a http put-request.
 *
 * @param url the url to request.
 * @param object the object to put.
 * @return a promise containing whether the put succeeded or not.
 */
export function putRequest<T>(url: string, object: T): Promise<boolean> {
  const options = buildOptions("PUT", postHeaders, object);
  return fetch(url, options)
    .then((response) => response.ok)
    .catch(() => false);
}

/**
 * Make a http post-request.
 *
 * @param url the url to request.
 * @param object the object to post.
 * @return a promise containing the result of the post request.
 */
export function postRequest<T, R>(url: string, object: T): Promise<R | undefined> {
  const options = buildOptions("POST", postHeaders, object);
  return handleFetch(fetch(url, options));
}

function handleFetch<T>(promise: Promise<Response>): Promise<T | undefined> {
  return promise
    .then((response) => {
      if (response.status === 200) {
        return response.json();
      } else {
        return undefined;
      }
    })
    .catch(() => undefined);
}
