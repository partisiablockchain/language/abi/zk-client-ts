/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Manually reconstructs a secret variable by fetching secret shares from ZK nodes. Main method
 * prints the value of a secret variable in a default contract on the testnet.
 */

import { BitInput } from "@secata-public/bitmanipulation-ts";
import { BinarySecretShares, BlockchainAddress } from "../../main";
import { Client } from "./util/Client";
import { RealClient } from "./util/RealClient";

/** Address of the testnet node to request shares from. */
const TESTNET_URL = "https://node1.testnet.partisiablockchain.com";

/**
 * Private key of the account interacting with the blockchain. This account should have enough gas
 * on the testnet.
 */
const SENDER_PRIVATE_KEY = "bb";

/**
 * Address of the zk contract to input to. Currently set to an immediate open contract which opens
 * any zk input it receives and saves it in the contract state.

 * <p><a href=
 * "https://testnet.partisiablockchain.com/info/contract/03e3914a2a884bac9072805409d44284e911e24f42"
 * >Link to deployed contract</a>, on testnet.
 */
const ZK_CONTRACT = BlockchainAddress.fromString("03e3914a2a884bac9072805409d44284e911e24f42");

/**
 * ID of the variable to reconstruct. This variable must be owned by SENDER_PRIVATE_KEY for the
 * manual test to work.
 */
const VARIABLE_ID = 17;

/**
 * Starts a blockchain client for the testnet then reconstructs a secret variable in a contract
 * using default values.
 */
async function main() {
  // Starts a blockchain client for the testnet
  const client = new Client(TESTNET_URL);
  const zkContract = await client.getContractState(ZK_CONTRACT);
  if (zkContract === undefined) {
    throw new Error("Couldn't get contract data");
  }

  // Gets the shares from a default ZK contract.
  const realClient: RealClient = RealClient.create(zkContract);
  const secretShares: BinarySecretShares = await realClient.getSharesFromEngines(
    ZK_CONTRACT,
    SENDER_PRIVATE_KEY,
    VARIABLE_ID
  );

  // Then reconstructs the secret variable.
  const reconstructedSecret = secretShares.reconstructSecret();
  const reconstructedInt = new BitInput(reconstructedSecret.data).readSignedNumber(32);

  // eslint-disable-next-line no-console
  console.log(reconstructedInt);
}

main();
