/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Manually creates zk input and sends it off-chain to a contract. Main method sends a default input
 * off-chain to a specific contract on the testnet.
 */

import { BitOutput } from "@secata-public/bitmanipulation-ts";
import BN from "bn.js";
import { BlockchainAddress, ZkRpcBuilder } from "../../main";
import { Client } from "./util/Client";
import { TransactionSender } from "./util/TransactionSender";
import { RealClient } from "./util/RealClient";

/** Address of the testnet node receiving the input. */
const TESTNET_URL = "https://node1.testnet.partisiablockchain.com";

/**
 * Private key of the account interacting with the blockchain. This account should have enough gas
 * on the testnet.
 */
const SENDER_PRIVATE_KEY = "bb";

/**
 * Address of the zk contract to input to. Currently set to an immediate open contract which opens
 * any zk input it receives and saves it in the contract state.

 * <p><a href=
 * "https://testnet.partisiablockchain.com/info/contract/03e3914a2a884bac9072805409d44284e911e24f42"
 * >Link to deployed contract</a>, on testnet.
 */
const ZK_CONTRACT = BlockchainAddress.fromString("03e3914a2a884bac9072805409d44284e911e24f42");

/** Identifier of the secret input action in the zk contract. */
const SECRET_INPUT_INVOCATION = 0x40;

/** The secret input value. */
const SECRET_INPUT_VALUE = 1000;

/**
 * Starts a blockchain client for the testnet then sends ZK input to a contract using default
 * values.
 */
async function main() {
  // Starts a blockchain client for the testnet.
  const client = new Client(TESTNET_URL);

  const zkContract = await client.getContractState(ZK_CONTRACT);
  if (zkContract === undefined) {
    throw new Error("Couldn't get contract data");
  }

  // Creates the RPC payload of an off chain secret input
  const serializedInput = BitOutput.serializeBits((out) =>
    out.writeSignedNumber(SECRET_INPUT_VALUE, 32)
  );
  const additionalRpc = Buffer.from([SECRET_INPUT_INVOCATION]);
  const payload = ZkRpcBuilder.zkInputOffChain(serializedInput, additionalRpc);

  // Sends the transaction to the default ZK contract.
  const transactionSender = TransactionSender.create(client, SENDER_PRIVATE_KEY);

  const sentTransaction = await transactionSender.sendAndSign(
    { address: ZK_CONTRACT, rpc: payload.rpc },
    new BN(100000)
  );

  const txIdentifier = sentTransaction.transactionPointer.identifier;
  // eslint-disable-next-line no-console
  console.log("Sent input in transaction: " + txIdentifier.toString("hex"));

  // Sends the blinded secret shares to the nodes directly.
  const realClient = RealClient.create(zkContract);

  const succeeded = await realClient.sendSharesToNodes(
    ZK_CONTRACT,
    transactionSender.getAddress(),
    txIdentifier,
    payload.blindedShares
  );

  if (succeeded) {
    // eslint-disable-next-line no-console
    console.log("Successfully sent shares");
  } else {
    // eslint-disable-next-line no-console
    console.log("Failed to send shares");
  }
}

main();
