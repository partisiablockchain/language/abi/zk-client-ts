/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { BigEndianByteInput, BitOutput } from "@secata-public/bitmanipulation-ts";
import { BlindedShares, CryptoUtils } from "../main";
import { ZkRpcBuilder } from "../main";
import { BinarySecretShares } from "../main";

describe("off chain tests", () => {
  /// Parameters used to generate the reference rpc
  const REFERENCE_SECRET = 13;
  const REFERENCE_INVOCATION = 0x40;

  const referenceRpc = Buffer.from(
    "040000000100000020957976d3afcf19d3a6d011b059400830" +
      "1342927f849ac5194b96ded06a93936028986bfc82b5a5c4ed" +
      "48ea3194139a950aa763b836f02b7b75e9877fb06c5457487e" +
      "78d895d0e6f5fc46616050d8237c42e5bd53464d192d5f00ae" +
      "29ba39a6f90e683b8294bdb3115aca142c94d16f876924939f" +
      "4f703616645e0d5a3835e99a40",
    "hex"
  );

  const rngHex =
    "d63122788e897f649bfababc3c2dc9e0c67e09cf92df50a839fb426efe9ee30b" +
    "8bf98dea532fd86e6e1bd259971406efe02e1a174e362ae464b62e8ed9859472" +
    "88744f1a8c40e6d2e777c1f10dc245290dfeb531389493a4486ce640e7c2e8f6" +
    "ad8831b250242995dd1a1e0cd89ab20f4d7b623388630d7b1829a3d98fdce863" +
    "ad91e4656ee18539d49a4e3bc2764c29dc86f027ce15a183f2430f112d087084";

  const referenceInput = BitOutput.serializeBits((s) => s.writeSignedNumber(REFERENCE_SECRET, 32));

  const referenceAdditionalRpc = Buffer.from([REFERENCE_INVOCATION]);

  function referenceRng(): (ln: number) => Buffer {
    // Same randomness as in the java test
    const out = new BigEndianByteInput(Buffer.from(rngHex, "hex"));
    return (ln: number) => out.readBytes(ln);
  }

  test("rpc matches reference", () => {
    const generatedRpc = ZkRpcBuilder.zkInputOffChain(
      referenceInput,
      referenceAdditionalRpc,
      referenceRng()
    ).rpc;

    expect(generatedRpc).toEqual(referenceRpc);
  });

  test("different rpc for different input", () => {
    const someZkRpc = ZkRpcBuilder.zkInputOffChain(
      referenceInput,
      referenceAdditionalRpc,
      referenceRng()
    ).rpc;

    const newInt = 41;
    const differentInput = BitOutput.serializeBits((s) => s.writeSignedNumber(newInt, 32));

    const withDifferentSecret = ZkRpcBuilder.zkInputOffChain(
      differentInput,
      referenceAdditionalRpc,
      referenceRng()
    ).rpc;
    expect(someZkRpc).not.toEqual(withDifferentSecret);

    const differentShortname = 0x41;
    const differentAdditionalRpc = Buffer.from([differentShortname]);

    const withDifferentInvocation = ZkRpcBuilder.zkInputOffChain(
      referenceInput,
      differentAdditionalRpc,
      referenceRng()
    ).rpc;
    expect(someZkRpc).not.toEqual(withDifferentInvocation);
  });

  test("test bit lengths", () => {
    const rng = referenceRng();
    const secretShares = BinarySecretShares.create(referenceInput);
    const commitments = secretShares.applyBlinding(rng).hash();
    const serializedCommitments = commitments.serialize();
    const expectedBitLength = 32 * secretShares.noOfShares();
    expect(serializedCommitments.length).toEqual(expectedBitLength);
  });

  test("test blinded shares against payload", () => {
    const offChainInput = ZkRpcBuilder.zkInputOffChain(referenceInput, referenceAdditionalRpc);

    const inputStream = new BigEndianByteInput(offChainInput.rpc);
    inputStream.readU8(); // read invocation
    const bitLengths = inputStream.readI32();
    for (let i = 0; i < bitLengths; i++) {
      inputStream.readI32(); // read bit length
    }

    const blindedShares = offChainInput.blindedShares.shares;
    expect(blindedShares).toHaveLength(4);
    for (const share of blindedShares) {
      const fromStream = inputStream.readBytes(32);
      const fromBlindedShares = CryptoUtils.hashBuffer(share);
      expect(fromStream).toEqual(fromBlindedShares);
    }
    // last byte should be invocation identifier
    expect(inputStream.readU8()).toEqual(REFERENCE_INVOCATION);
  });

  test("blinded shares export", () => {
    expect(BlindedShares.name).toEqual("BlindedShares");
  });
});
