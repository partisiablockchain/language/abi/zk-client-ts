/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { BigEndianByteInput, BitOutput } from "@secata-public/bitmanipulation-ts";
import { CryptoUtils } from "../main";
import { BlockchainPublicKey } from "../main";
import { ZkRpcBuilder } from "../main";
import { BlockchainAddress } from "../main";

describe("on chain tests", () => {
  /// Parameters used to generate the reference rpc
  const REFERENCE_SECRET = 13;
  const REFERENCE_INVOCATION = 0x40;
  const REFERENCE_SENDER = "bb";
  const REFERENCE_ENCRYPTION_SECRET_KEY = "beedad";

  /// Testnet engines (as base64 string)
  const ENGINE_1 = "Ax3kZlMV9JW6EE/74YO9X8Y7zVeD8TubNlBaY+IMfARg";
  const ENGINE_2 = "Awndg7zaEkVJxHgNzaITUUX+uzAXYl9Ja9X8QPcRy9pZ";
  const ENGINE_3 = "A9SqNrfygSuXOLNsdy4Gx8d0kSV5S/ET7GCnTVz90FQ7";
  const ENGINE_4 = "Ari1IPP44JCkTfB98nVyMn0y2bZwgkQEEvrBV/umVkUo";

  const referenceRpc = Buffer.from(
    "05000000010000002002a755b11067317347cb614f1879b0b9" +
      "339adb5d1bf1b8694611707b176d1a82630000010090df042b" +
      "000738a6911e415984a2b6db005e773aebf2e1c278dae8fad0" +
      "b30188dc5d0d5646c8d3f52eda168de28858d3aac375495d26" +
      "865931ed8ce0849d457643fc7e627274d062fbcb9853642821" +
      "c4f0b56b6691dbd5bac47eb0c70e377d1b833168d61bd937df" +
      "7d2a484cc846aef9372f0a2e1634bb68ca03c46821918fa095" +
      "724bd0a3fd90ef13b2ca0d95a59012f7b2c5c14d913a483cc9" +
      "64823aead46937a19084732f2fe6fea81d2b8292f6f74d328f" +
      "1c53c9c53985752ce0810690d9dd476ddba9aae893fbb8cfb4" +
      "93aa9410fc2d10667d29d817eb85ca05d59e6cdc515e9ffd49" +
      "514c7210d47c7cb35590ec26279b4bdb05332aee69192647a0" +
      "3d6640",
    "hex"
  );

  const referenceSender = BlockchainAddress.fromString(
    CryptoUtils.privateKeyToAccountAddress(REFERENCE_SENDER)
  );

  const referenceInput = BitOutput.serializeBits((s) => s.writeSignedNumber(REFERENCE_SECRET, 32));

  const referenceAdditionalRpc = Buffer.from([REFERENCE_INVOCATION]);

  const referenceEngineKeys = [
    BlockchainPublicKey.fromBuffer(Buffer.from(ENGINE_2, "base64")),
    BlockchainPublicKey.fromBuffer(Buffer.from(ENGINE_4, "base64")),
    BlockchainPublicKey.fromBuffer(Buffer.from(ENGINE_3, "base64")),
    BlockchainPublicKey.fromBuffer(Buffer.from(ENGINE_1, "base64")),
  ];

  const referenceEncryptionKey = CryptoUtils.privateKeyToKeypair(REFERENCE_ENCRYPTION_SECRET_KEY);

  function referenceRng(): (ln: number) => Buffer {
    // Same randomness as in the java test
    const randomHex = "d63122788e897f649bfababc3c2dc9e0c67e09cf92df50a839fb426efe9ee30b";
    const out = new BigEndianByteInput(Buffer.from(randomHex, "hex"));
    return (ln: number) => out.readBytes(ln);
  }

  test("rpc matches reference", () => {
    const generatedRpc = ZkRpcBuilder.zkInputOnChain(
      referenceSender,
      referenceInput,
      referenceAdditionalRpc,
      referenceEngineKeys,
      referenceEncryptionKey,
      referenceRng()
    );

    expect(generatedRpc).toEqual(referenceRpc);
  });

  test("different rpc for different input", () => {
    const somZkRpc = ZkRpcBuilder.zkInputOnChain(
      referenceSender,
      referenceInput,
      referenceAdditionalRpc,
      referenceEngineKeys,
      referenceEncryptionKey,
      referenceRng()
    );

    const differentKey = CryptoUtils.generateKeyPair();
    const differentSender = BlockchainAddress.fromString(
      CryptoUtils.keyPairToAccountAddress(differentKey)
    );

    const withDifferentSender = ZkRpcBuilder.zkInputOnChain(
      differentSender,
      referenceInput,
      referenceAdditionalRpc,
      referenceEngineKeys,
      referenceEncryptionKey,
      referenceRng()
    );
    expect(somZkRpc).not.toEqual(withDifferentSender);

    const differentInput = BitOutput.serializeBits((out) => out.writeSignedNumber(41, 32));
    const withDifferentSecret = ZkRpcBuilder.zkInputOnChain(
      differentSender,
      differentInput,
      referenceAdditionalRpc,
      referenceEngineKeys,
      referenceEncryptionKey,
      referenceRng()
    );
    expect(somZkRpc).not.toEqual(withDifferentSecret);

    const differentAdditionRpc = Buffer.from([0x41]);
    const withDifferentInvocation = ZkRpcBuilder.zkInputOnChain(
      differentSender,
      differentInput,
      differentAdditionRpc,
      referenceEngineKeys,
      referenceEncryptionKey,
      referenceRng()
    );
    expect(somZkRpc).not.toEqual(withDifferentInvocation);

    const differentEngines = referenceEngineKeys.slice();
    differentEngines[0] = BlockchainPublicKey.fromString(
      CryptoUtils.generateKeyPair().getPublic().encodeCompressed("hex")
    );
    const withDifferentEngineKeys = ZkRpcBuilder.zkInputOnChain(
      differentSender,
      differentInput,
      differentAdditionRpc,
      differentEngines,
      referenceEncryptionKey,
      referenceRng()
    );
    expect(somZkRpc).not.toEqual(withDifferentEngineKeys);

    const withDifferentEncryptionKey = ZkRpcBuilder.zkInputOnChain(
      differentSender,
      differentInput,
      differentAdditionRpc,
      referenceEngineKeys,
      differentKey,
      referenceRng()
    );
    expect(somZkRpc).not.toEqual(withDifferentEncryptionKey);
  });

  test("Different rpc for same input", () => {
    const secretInt = 42;
    const binaryInput = BitOutput.serializeBits((out) => out.writeSignedNumber(secretInt, 32));

    const someZkRpc = ZkRpcBuilder.zkInputOnChain(
      referenceSender,
      binaryInput,
      referenceAdditionalRpc,
      referenceEngineKeys
    );
    const differentZkRpc = ZkRpcBuilder.zkInputOnChain(
      referenceSender,
      binaryInput,
      referenceAdditionalRpc,
      referenceEngineKeys
    );

    expect(someZkRpc).not.toEqual(differentZkRpc);
  });
});
