/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { Cipher, createDecipheriv, randomBytes } from "crypto";
import {
  BigEndianByteInput,
  BigEndianByteOutput,
  BitOutput,
} from "@secata-public/bitmanipulation-ts";
import { ec as Elliptic } from "elliptic";
import { BinaryExtensionFieldElement, BinarySecretShares } from "../main";
import { Share } from "../main/BinarySecretShares";
import { CryptoUtils } from "../main";
import { BlockchainAddress } from "../main";
import { BlockchainPublicKey } from "../main";

test("incorrect number of shares", () => {
  const tooManyEngines = [
    BlockchainPublicKey.fromString(
      CryptoUtils.generateKeyPair().getPublic().encodeCompressed("hex")
    ),
    BlockchainPublicKey.fromString(
      CryptoUtils.generateKeyPair().getPublic().encodeCompressed("hex")
    ),
    BlockchainPublicKey.fromString(
      CryptoUtils.generateKeyPair().getPublic().encodeCompressed("hex")
    ),
    BlockchainPublicKey.fromString(
      CryptoUtils.generateKeyPair().getPublic().encodeCompressed("hex")
    ),
    BlockchainPublicKey.fromString(
      CryptoUtils.generateKeyPair().getPublic().encodeCompressed("hex")
    ),
  ];
  const binaryInput = BitOutput.serializeBits((out) => out.writeSignedNumber(5, 32));
  const shares = BinarySecretShares.create(binaryInput);
  expect(() =>
    shares.encrypt(
      CryptoUtils.generateKeyPair(),
      BlockchainAddress.fromString(
        CryptoUtils.keyPairToAccountAddress(CryptoUtils.generateKeyPair())
      ),
      tooManyEngines
    )
  ).toThrowError("Number of engines (5) did not match number of shares (4)");
});

test("read from stream", () => {
  const share = BigEndianByteOutput.serialize((out) => {
    out.writeI32(2); // number of elements
    out.writeI32(1); // bit length of first element
    out.writeBytes(Buffer.from([10])); // shares of first element
    out.writeI32(2); // bit length of second element
    out.writeBytes(Buffer.from([-11, 12])); // shares of second element
  });

  const shares = [share, share, share, share];
  const readShares = BinarySecretShares.read(shares);

  const expectedShares = new BinarySecretShares([
    shareFromBytes(Buffer.from([10, -11, 12])),
    shareFromBytes(Buffer.from([10, -11, 12])),
    shareFromBytes(Buffer.from([10, -11, 12])),
    shareFromBytes(Buffer.from([10, -11, 12])),
  ]);

  expect(readShares.isEqualTo(expectedShares)).toEqual(true);
});

test("Sames shares with same randomness", () => {
  const input = 13;
  const randomBuffer = randomBytes(32);
  const rand1 = new BigEndianByteInput(randomBuffer);
  const random1 = (ln: number) => rand1.readBytes(ln);
  const rand2 = new BigEndianByteInput(randomBuffer);
  const random2 = (ln: number) => rand2.readBytes(ln);

  const secretVarData1 = BitOutput.serializeBits((out) => out.writeSignedNumber(input, 16));
  const secretVarData2 = BitOutput.serializeBits((out) => out.writeSignedNumber(input, 16));

  const secretShares1 = BinarySecretShares.create(secretVarData1, random1);
  const secretShares2 = BinarySecretShares.create(secretVarData2, random2);

  expect(secretShares1).toEqual(secretShares2);
});

test("Different shares with different randomness", () => {
  const input = 13;

  const secretVarData1 = BitOutput.serializeBits((out) => out.writeSignedNumber(input, 16));
  const secretVarData2 = BitOutput.serializeBits((out) => out.writeSignedNumber(input, 16));

  const secretShares1 = BinarySecretShares.create(secretVarData1);
  const secretShares2 = BinarySecretShares.create(secretVarData2);

  expect(secretShares1).not.toEqual(secretShares2);
});

test("encrypt and decrypt shares", () => {
  const shares = [
    shareFromBytes(Buffer.from([1, 2])),
    shareFromBytes(Buffer.from([3, 4])),
    shareFromBytes(Buffer.from([5, 6])),
    shareFromBytes(Buffer.from([7, 8])),
  ];
  const secretShares = new BinarySecretShares(shares);
  const ephemeralKey = CryptoUtils.generateKeyPair();
  const engineKeys = [
    CryptoUtils.generateKeyPair(),
    CryptoUtils.generateKeyPair(),
    CryptoUtils.generateKeyPair(),
    CryptoUtils.generateKeyPair(),
  ];
  const enginePublicKeys = engineKeys.map((kp) =>
    BlockchainPublicKey.fromString(kp.getPublic(true, "hex"))
  );

  const senderAddress = BlockchainAddress.fromString(
    CryptoUtils.keyPairToAccountAddress(CryptoUtils.generateKeyPair())
  );

  const encryptedShares = secretShares.encrypt(ephemeralKey, senderAddress, enginePublicKeys);

  for (let i = 0; i < 4; i++) {
    const aes = createAesDecipherForParty(
      engineKeys[i],
      Buffer.from(ephemeralKey.getPublic(true, "hex"), "hex")
    );
    const shareToDecrypt = encryptedShares.shares[i];
    const decryptedBytes = Buffer.concat([aes.update(shareToDecrypt), aes.final()]);

    const rawShare = decryptedBytes.slice(0, 2);
    const decryptedShare = shareFromBytes(rawShare);
    expect(decryptedShare.isEqualTo(shares[i])).toEqual(true);

    const decryptedAddress = BlockchainAddress.fromBuffer(decryptedBytes.slice(2));
    expect(decryptedAddress).toEqual(senderAddress);
  }
});

test("isEqualTo", () => {
  const shares1 = new BinarySecretShares([
    shareFromBytes(Buffer.from([10, -11, 12])),
    shareFromBytes(Buffer.from([10, -11, 12])),
  ]);

  const shares2 = new BinarySecretShares([
    shareFromBytes(Buffer.from([10, -11, 12])),
    shareFromBytes(Buffer.from([10, -11, 12])),
  ]);

  const shares3 = new BinarySecretShares([shareFromBytes(Buffer.from([10, -11, 12]))]);

  const shares4 = new BinarySecretShares([
    shareFromBytes(Buffer.from([10, -11])),
    shareFromBytes(Buffer.from([10, -11, 12])),
  ]);

  const shares5 = new BinarySecretShares([
    shareFromBytes(Buffer.from([10, -11, 42])),
    shareFromBytes(Buffer.from([10, -11, 12])),
  ]);

  expect(shares1.isEqualTo(shares2)).toEqual(true);
  expect(shares1.isEqualTo(shares3)).toEqual(false);
  expect(shares1.isEqualTo(shares4)).toEqual(false);
  expect(shares1.isEqualTo(shares5)).toEqual(false);
});

function shareFromBytes(toConvert: Buffer): Share {
  const elements: BinaryExtensionFieldElement[] = [];
  toConvert.forEach((b) => elements.push(BinaryExtensionFieldElement.createElement(b)));
  return new Share(elements);
}

function createAesDecipherForParty(keyPair: Elliptic.KeyPair, publicKey: Buffer): Cipher {
  const sharedKey = CryptoUtils.createSharedKey(keyPair, publicKey);
  const iv = sharedKey.slice(0, 16);
  const secretKey = sharedKey.slice(16, 32);
  return createDecipheriv("aes-128-cbc", secretKey, iv);
}
