/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { randomBytes } from "crypto";
import { BigEndianByteInput, BitOutput, CompactBitArray } from "@secata-public/bitmanipulation-ts";
import { ec as Elliptic } from "elliptic";
import { BinaryExtensionFieldElement, Lagrange, Polynomial } from "../main";
import { EncryptedShare, EncryptedShares } from "./EncryptedShares";
import { BlockchainAddress } from "./util/BlockchainAddress";
import { BlockchainPublicKey } from "./util/BlockchainPublicKey";
import { CryptoUtils } from "./util/CryptoUtils";
import { BlindedShare, BlindedShares } from "./BlindedShares";
import { Blinding, Blindings } from "./Blindings";

/**
 * Binary data which has been broken into secret shares. BinarySecretShares are distributed among ZK
 * nodes when inputting a secret variable, and received from ZK nodes when reconstructing a secret
 * variable.
 */
export class BinarySecretShares {
  private readonly secretShares: Share[];

  static readonly ALPHAS = BinaryExtensionFieldElement.computationAlphas();

  /**
   * Constructs secret shares from a list of shares. The list contains a share for each
   * sending/receiving party of the secret data.
   *
   * @param secretShares the list of shares
   */
  constructor(secretShares: Share[]) {
    this.secretShares = secretShares;
  }

  /**
   * Creates binary secret shares from binary secret data. For each secret bit a random polynomial
   * of degree 1 is created with the secret bit embedded as the constant term. To create each share
   * of the bit the polynomial is evaluated at a point.
   *
   * @param variableData binary secret variable data to create the secret shares from
   * @param rng randomness generator used to generate random polynomials
   * @return the created shares.
   */
  public static create(
    variableData: CompactBitArray,
    rng?: (ln: number) => Buffer
  ): BinarySecretShares {
    const random = rng ?? randomBytes;

    const sharedElements: BinaryExtensionFieldElement[][] = [];
    for (let i = 0; i < this.ALPHAS.length; i++) {
      sharedElements.push([]);
    }

    for (let i = 0; i < variableData.length; i++) {
      const secret = this.isBitSet(variableData.data, i)
        ? BinaryExtensionFieldElement.ONE
        : BinaryExtensionFieldElement.ZERO;

      const polynomial = this.generatePolynomial(secret, random);

      for (let j = 0; j < this.ALPHAS.length; j++) {
        const share = polynomial.evaluate(this.ALPHAS[j]);
        sharedElements[j].push(share);
      }
    }

    return new BinarySecretShares(sharedElements.map((s) => new Share(s)));
  }

  /**
   * Create binary secret shares from bytes read from ZK nodes.
   *
   * @param rawShares list of raw shares read from ZK nodes
   * @return the created shares
   */
  public static read(rawShares: Buffer[]): BinarySecretShares {
    const readShares = rawShares.map((s) => Share.read(s));
    return new BinarySecretShares(readShares);
  }

  /**
   * Generates a random polynomial of degree 1:
   *
   * <p><i>f(x)= secret + random*x</i>
   *
   * <p>such that f(0) match the provided secret.
   *
   * @param secret the secret to be embedded in the constant term
   * @param rng randomness generator used to generate random byte
   * @return a random polynomial generated with the secret and a random number as coefficients.
   */
  private static generatePolynomial(
    secret: BinaryExtensionFieldElement,
    rng: (ln: number) => Buffer
  ): Polynomial<BinaryExtensionFieldElement> {
    const randomByte = rng(1)[0];
    const random = BinaryExtensionFieldElement.createElement(randomByte);
    return BinaryExtensionFieldElement.createPoly([secret, random]);
  }

  private static isBitSet(data: Buffer, index: number): boolean {
    const byteIndex = Math.floor(index / 8);
    const byteValue = data[byteIndex];
    const bitIndex = index % 8;
    const bitValue = byteValue & (1 << bitIndex);
    return bitValue !== 0;
  }

  /**
   * Reconstruct the secret variable data from these BinarySecretShares. First the shares of each
   * bit of the secret variable data is grouped. Then, a polynomial is interpolated from the shares
   * of each bit. The constant term of this polynomial is the value of the secret bit. Lastly, the
   * secret bits are collected in a byte array to form the secret variable data.
   *
   * @return the reconstructed binary secret variable data
   */
  public reconstructSecret(): CompactBitArray {
    return BitOutput.serializeBits((out) => {
      for (let i = 0; i < this.getBitLength(); i++) {
        out.writeBoolean(this.reconstructSecretBit(i));
      }
    });
  }

  /**
   * Reconstructs one bit of the secret variable data which these binary secret shares constitute. A
   * polynomial is interpolated from the shares of the bit, and the constant term of this polynomial
   * is returned as the reconstructed bit.
   *
   * @param i index of the bit to reconstruct
   * @return the reconstructed bit
   */
  private reconstructSecretBit(i: number): boolean {
    const sharesOfBit = this.secretShares.map((share) => share.bitElements[i]);
    const polynomial = Lagrange.interpolateCheckDegree(
      BinarySecretShares.ALPHAS,
      sharesOfBit,
      1,
      BinaryExtensionFieldElement.ZERO,
      BinaryExtensionFieldElement.ONE
    );
    return !polynomial.getConstantTerm().isZero();
  }

  private getBitLength() {
    return this.secretShares[0].bitElements.length;
  }

  /**
   * Encrypts the secret shares to prepare them for sending on-chain. Each share is encrypted using
   * a shared key. The shared key is created from an ephemeral (temporary) private key and the
   * public key of the receiving engine. This allows only the receiving engine to recover the value
   * of the share. The private key is ephemeral such that two identical shares will be encrypted
   * differently in order to prevent replay attacks.
   *
   * @param ephemeralKey The temporary key to encrypt the shares with
   * @param sender the address of sender of the secret input
   * @param engineKeys the keys of the engines that are to receive the encrypted shares
   * @return the encrypted shares
   */
  public encrypt(
    ephemeralKey: Elliptic.KeyPair,
    sender: BlockchainAddress,
    engineKeys: BlockchainPublicKey[]
  ): EncryptedShares {
    if (this.secretShares.length !== engineKeys.length) {
      throw new Error(
        `Number of engines (${engineKeys.length}) did not match number of shares (${this.secretShares.length})`
      );
    }
    const shares = [];
    for (let i = 0; i < engineKeys.length; i++) {
      const share = this.secretShares[i];
      const engineKey = engineKeys[i];
      const encryptedShare = share.encrypt(ephemeralKey, sender, engineKey);
      shares.push(encryptedShare);
    }
    return new EncryptedShares(shares);
  }

  /**
   * Applies blinding to the shares to prepare them for sending off-chain. If blinding were not
   * applied, secret shares could easily be guessed for small input sizes, since hashes of the
   * shares are put directly on the blockchain.
   *
   * @param rng used to generate blindings
   * @return the shares concatenated with blindings
   */
  public applyBlinding(rng: (ln: number) => Buffer): BlindedShares {
    const blindings = Blindings.generate(this.noOfShares(), rng);
    const blindedShares: BlindedShare[] = [];
    for (let i = 0; i < blindings.blindings.length; i++) {
      const share = this.secretShares[i];
      const blinding = blindings.blindings[i];
      const blindedShare = share.serializeWithBlinding(blinding);
      blindedShares.push(blindedShare);
    }
    return new BlindedShares(blindedShares);
  }

  /**
   * Returns the amount of shares.
   *
   * @return the size of the share list
   */
  public noOfShares(): number {
    return this.secretShares.length;
  }

  isEqualTo(that: BinarySecretShares): boolean {
    if (that.secretShares.length !== this.secretShares.length) {
      return false;
    }
    for (let i = 0; i < this.secretShares.length; i++) {
      if (!that.secretShares[i].isEqualTo(this.secretShares[i])) {
        return false;
      }
    }
    return true;
  }
}

/**
 * Secret shares are disjoint parts of a secret input. A secret share consists of a list of
 * BinaryExtensionFieldElements which are in turn secret shares of a single bit.
 */
export class Share {
  readonly bitElements: BinaryExtensionFieldElement[];

  /**
   * Constructs a secret share from a list of bit elements.
   *
   * @param bitElements the bit elements that constitutes the share
   */
  constructor(bitElements: BinaryExtensionFieldElement[]) {
    this.bitElements = bitElements;
  }

  /**
   * Read a binary share from a stream, in the format specified by the ZK nodes. This is used when
   * shares are fetched from ZK nodes during reconstruction of a secret variable.
   *
   * @param shareBytes the buffer to read the share from
   * @return the read share
   */
  public static read(shareBytes: Buffer): Share {
    const stream = new BigEndianByteInput(shareBytes);
    const numOfElements = stream.readI32();
    const sharesOfBits: BinaryExtensionFieldElement[] = [];
    for (let i = 0; i < numOfElements; i++) {
      const bitLength = stream.readI32();
      for (let j = 0; j < bitLength; j++) {
        const shareOfBit = BinaryExtensionFieldElement.createElement(stream.readI8());
        sharesOfBits.push(shareOfBit);
      }
    }
    return new Share(sharesOfBits);
  }

  /**
   * Encrypts a share using a shared key. The shared key is created from an ephemeral (temporary)
   * private key and the public key of the receiving party. This allows only the receiving party
   * to recover the value of the share. The private key is ephemeral such that two identical
   * shares will be encrypted differently in order to prevent replay attacks. The identity
   * (address) of the sender is encrypted along with the share such that a third party cannot
   * spoof the identity of the sender.
   *
   * @param ephemeralKey The temporary key to encrypt the share with
   * @param sender the address of the sender of the secret input
   * @param enginePublicKey the key of the engine that is to receive the encrypted share
   * @return the encrypted share
   */
  public encrypt(
    ephemeralKey: Elliptic.KeyPair,
    sender: BlockchainAddress,
    enginePublicKey: BlockchainPublicKey
  ): EncryptedShare {
    const aes = CryptoUtils.createAesForParty(ephemeralKey, enginePublicKey.asBuffer());
    return Buffer.concat([
      aes.update(this.serialize()),
      aes.update(sender.asBuffer()),
      aes.final(),
    ]);
  }

  /**
   * Serializes each bit element of the share.
   *
   * @return the serialized share
   */
  private serialize(): Buffer {
    return Buffer.from(this.bitElements.map((b) => b.value));
  }

  /**
   * Applies the supplied blinding by serializing this share along with the blinding.
   *
   * @param blinding the blinding to apply
   * @return the serialized share and blinding
   */
  serializeWithBlinding(blinding: Blinding): Buffer {
    return Buffer.concat([this.serialize(), blinding]);
  }

  isEqualTo(that: Share): boolean {
    if (that.bitElements.length !== this.bitElements.length) {
      return false;
    }
    for (let i = 0; i < this.bitElements.length; i++) {
      if (!that.bitElements[i].isEqualTo(this.bitElements[i])) {
        return false;
      }
    }
    return true;
  }
}
