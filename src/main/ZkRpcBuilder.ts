/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { randomBytes } from "crypto";
import { BigEndianByteOutput, CompactBitArray } from "@secata-public/bitmanipulation-ts";
import { ec as Elliptic } from "elliptic";
import { BlockchainAddress } from "./util/BlockchainAddress";
import { BlockchainPublicKey } from "./util/BlockchainPublicKey";
import { CryptoUtils } from "./util/CryptoUtils";
import { BinarySecretShares } from "./BinarySecretShares";
import { BlindedShares } from "./BlindedShares";

const ZERO_KNOWLEDGE_INPUT_OFF_CHAIN = 0x04;
const ZERO_KNOWLEDGE_INPUT_ON_CHAIN = 0x05;

/**
 * Build the rpc for inputting secret binary data on-chain to a ZK contract, using a supplied key
 * pair and random number generator.
 *
 * @param sender the sender of the input, which is encrypted along with the input data
 * @param secretInput the binary input to create rpc from
 * @param additionalRpc additional rpc to send along with transaction. Must as a minimum include
 *     the zk input shortname
 * @param engineKeys the public keys of the ZK engines associated with the ZK contract
 * @param encryptionKey the key pair used for encrypting the secret
 * @param rng randomness generator used when generating shares of the secret
 * @return the rpc
 */
function zkInputOnChain(
  sender: BlockchainAddress,
  secretInput: CompactBitArray,
  additionalRpc: Buffer,
  engineKeys: BlockchainPublicKey[],
  encryptionKey?: Elliptic.KeyPair,
  rng?: (ln: number) => Buffer
): Buffer {
  const actualEncryptionKey = encryptionKey ?? CryptoUtils.generateKeyPair();
  const actualRng = rng ?? randomBytes;

  const secretShares = BinarySecretShares.create(secretInput, actualRng);
  const encryptedShares = secretShares.encrypt(actualEncryptionKey, sender, engineKeys);
  const concatenatedEncryptedShares = Buffer.concat(encryptedShares.shares);

  return BigEndianByteOutput.serialize((out) => {
    out.writeU8(ZERO_KNOWLEDGE_INPUT_ON_CHAIN);

    const bitLengths = [secretInput.length];
    out.writeI32(bitLengths.length);
    for (const bitLength of bitLengths) {
      out.writeI32(bitLength);
    }

    out.writeBytes(Buffer.from(actualEncryptionKey.getPublic().encodeCompressed()));
    out.writeI32(concatenatedEncryptedShares.length);
    out.writeBytes(concatenatedEncryptedShares);
    out.writeBytes(additionalRpc);
  });
}

/**
 * Build the rpc for inputting secret binary data on-chain to a ZK contract, using a supplied
 * random number generator.
 *
 * @param secretInput the binary input to create rpc from
 * @param additionalRpc additional rpc to send along with transaction. Must as a minimum include
 *     the zk input shortname
 * @param rng randomness generator used when generating shares of the secret, as well as blindings
 * @return off-chain input consisting of an rpc to be sent to the real wasm binder and blinded
 *     shares to be sent to ZK nodes associated with the contract
 */
function zkInputOffChain(
  secretInput: CompactBitArray,
  additionalRpc: Buffer,
  rng?: (ln: number) => Buffer
): OffChainInput {
  const actualRng = rng ?? randomBytes;

  const secretShares = BinarySecretShares.create(secretInput, actualRng);
  const blindedShares = secretShares.applyBlinding(actualRng);
  const shareCommitments = blindedShares.hash();

  const rpc = BigEndianByteOutput.serialize((out) => {
    out.writeU8(ZERO_KNOWLEDGE_INPUT_OFF_CHAIN);
    const bitLengths = [secretInput.length];
    out.writeI32(bitLengths.length);
    for (const bitLength of bitLengths) {
      out.writeI32(bitLength);
    }
    out.writeBytes(shareCommitments.serialize());
    out.writeBytes(additionalRpc);
  });

  return { blindedShares, rpc };
}

export interface OffChainInput {
  blindedShares: BlindedShares;
  rpc: Buffer;
}

export const ZkRpcBuilder = {
  zkInputOnChain,
  zkInputOffChain,
};
