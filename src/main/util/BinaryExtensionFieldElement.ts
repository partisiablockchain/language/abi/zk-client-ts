/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { FiniteFieldElement } from "./FiniteFieldElement";
import { Polynomial } from "./Polynomial";

/**
 * A binary extension field element.
 */
export class BinaryExtensionFieldElement
  implements FiniteFieldElement<BinaryExtensionFieldElement>
{
  readonly value: number;

  private constructor(value: number) {
    this.value = value;
  }

  /**
   * The zero element.
   */
  static readonly ZERO: BinaryExtensionFieldElement = BinaryExtensionFieldElement.createElement(0);

  /**
   * The one element.
   */
  static readonly ONE: BinaryExtensionFieldElement = BinaryExtensionFieldElement.createElement(1);

  /**
   * Create a polynomial.
   * @param coefficients the coefficients of the polynomial
   */
  static createPoly(
    coefficients: BinaryExtensionFieldElement[]
  ): Polynomial<BinaryExtensionFieldElement> {
    return Polynomial.create(coefficients, this.ZERO);
  }

  /**
   * Create a new field element with the given value.
   * @param value the value
   */
  static createElement(value: number): BinaryExtensionFieldElement {
    return new BinaryExtensionFieldElement(value & 0b1111);
  }

  /**
   * Get the computation alphas for this field.
   * @returns the computation alphas
   */
  static computationAlphas(): readonly BinaryExtensionFieldElement[] {
    return Constants.computationAlphas;
  }

  /** @inheritDoc */
  add(other: BinaryExtensionFieldElement): BinaryExtensionFieldElement {
    return Constants.elements[other.value ^ this.value];
  }

  /** @inheritDoc */
  subtract(other: BinaryExtensionFieldElement): BinaryExtensionFieldElement {
    return Constants.elements[other.value ^ this.value];
  }

  /** @inheritDoc */
  multiply(other: BinaryExtensionFieldElement): BinaryExtensionFieldElement {
    const index = other.value * 16 + this.value;
    return Constants.elements[Constants.lookup[index]];
  }

  /** @inheritDoc */
  modInverse(): BinaryExtensionFieldElement {
    return Constants.elements[Constants.multiplicativeInverse[this.value]];
  }

  /** @inheritDoc */
  negate(): BinaryExtensionFieldElement {
    return this;
  }

  /** @inheritDoc */
  squareRoot(): BinaryExtensionFieldElement {
    return Constants.elements[Constants.squareRootLookUp[this.value]];
  }

  /** @inheritDoc */
  isZero(): boolean {
    return this.value === 0;
  }

  /** @inheritDoc */
  isOne(): boolean {
    return this.value === 1;
  }

  /** @inheritDoc */
  isEqualTo(that: BinaryExtensionFieldElement): boolean {
    return that.value === this.value;
  }
}

const Constants = {
  elements: [
    BinaryExtensionFieldElement.createElement(0),
    BinaryExtensionFieldElement.createElement(1),
    BinaryExtensionFieldElement.createElement(2),
    BinaryExtensionFieldElement.createElement(3),
    BinaryExtensionFieldElement.createElement(4),
    BinaryExtensionFieldElement.createElement(5),
    BinaryExtensionFieldElement.createElement(6),
    BinaryExtensionFieldElement.createElement(7),
    BinaryExtensionFieldElement.createElement(8),
    BinaryExtensionFieldElement.createElement(9),
    BinaryExtensionFieldElement.createElement(10),
    BinaryExtensionFieldElement.createElement(11),
    BinaryExtensionFieldElement.createElement(12),
    BinaryExtensionFieldElement.createElement(13),
    BinaryExtensionFieldElement.createElement(14),
    BinaryExtensionFieldElement.createElement(15),
  ],
  computationAlphas: [
    BinaryExtensionFieldElement.createElement(1),
    BinaryExtensionFieldElement.createElement(2),
    BinaryExtensionFieldElement.createElement(3),
    BinaryExtensionFieldElement.createElement(4),
  ],
  multiplicativeInverse: [
    0, 0x01, 0x09, 0x0e, 0x0d, 0x0b, 0x07, 0x06, 0x0f, 0x02, 0x0c, 0x05, 0x0a, 0x04, 0x03, 0x08,
  ],
  squareRootLookUp: [
    0, 0x01, 0x05, 0x04, 0x02, 0x03, 0x07, 0x06, 0x0a, 0x0b, 0x0f, 0x0e, 0x08, 0x09, 0x0d, 0x0c,
  ],
  lookup: [
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
    0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0, 0x02, 0x04, 0x06, 0x08, 0x0a, 0x0c, 0x0e,
    0x03, 0x01, 0x07, 0x05, 0x0b, 0x09, 0x0f, 0x0d, 0, 0x03, 0x06, 0x05, 0x0c, 0x0f, 0x0a, 0x09,
    0x0b, 0x08, 0x0d, 0x0e, 0x07, 0x04, 0x01, 0x02, 0, 0x04, 0x08, 0x0c, 0x03, 0x07, 0x0b, 0x0f,
    0x06, 0x02, 0x0e, 0x0a, 0x05, 0x01, 0x0d, 0x09, 0, 0x05, 0x0a, 0x0f, 0x07, 0x02, 0x0d, 0x08,
    0x0e, 0x0b, 0x04, 0x01, 0x09, 0x0c, 0x03, 0x06, 0, 0x06, 0x0c, 0x0a, 0x0b, 0x0d, 0x07, 0x01,
    0x05, 0x03, 0x09, 0x0f, 0x0e, 0x08, 0x02, 0x04, 0, 0x07, 0x0e, 0x09, 0x0f, 0x08, 0x01, 0x06,
    0x0d, 0x0a, 0x03, 0x04, 0x02, 0x05, 0x0c, 0x0b, 0, 0x08, 0x03, 0x0b, 0x06, 0x0e, 0x05, 0x0d,
    0x0c, 0x04, 0x0f, 0x07, 0x0a, 0x02, 0x09, 0x01, 0, 0x09, 0x01, 0x08, 0x02, 0x0b, 0x03, 0x0a,
    0x04, 0x0d, 0x05, 0x0c, 0x06, 0x0f, 0x07, 0x0e, 0, 0x0a, 0x07, 0x0d, 0x0e, 0x04, 0x09, 0x03,
    0x0f, 0x05, 0x08, 0x02, 0x01, 0x0b, 0x06, 0x0c, 0, 0x0b, 0x05, 0x0e, 0x0a, 0x01, 0x0f, 0x04,
    0x07, 0x0c, 0x02, 0x09, 0x0d, 0x06, 0x08, 0x03, 0, 0x0c, 0x0b, 0x07, 0x05, 0x09, 0x0e, 0x02,
    0x0a, 0x06, 0x01, 0x0d, 0x0f, 0x03, 0x04, 0x08, 0, 0x0d, 0x09, 0x04, 0x01, 0x0c, 0x08, 0x05,
    0x02, 0x0f, 0x0b, 0x06, 0x03, 0x0e, 0x0a, 0x07, 0, 0x0e, 0x0f, 0x01, 0x0d, 0x03, 0x02, 0x0c,
    0x09, 0x07, 0x06, 0x08, 0x04, 0x0a, 0x0b, 0x05, 0, 0x0f, 0x0d, 0x02, 0x09, 0x06, 0x04, 0x0b,
    0x01, 0x0e, 0x0c, 0x03, 0x08, 0x07, 0x05, 0x0a,
  ],
} as const;
