/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/** An address on the PBC blockchain. */
export class BlockchainAddress {
  private readonly val: Buffer;
  static readonly ADDRESS_LENGTH = 21;
  private constructor(val: Buffer) {
    this.val = val;
  }

  /**
   * Returns the blockchain address encoded as a hex string.
   *
   * @return the hex string.
   */
  public asString(): string {
    return this.val.toString("hex");
  }

  /**
   * Returns the blockchain address as bytes.
   *
   * @return the bytes.
   */
  public asBuffer(): Buffer {
    return Buffer.from(this.val);
  }

  /**
   * Creates an address from a hex string.
   *
   * @param val the encoded address.
   * @return the blockchain address.
   */
  public static fromString(val: string): BlockchainAddress {
    const buffer = Buffer.from(val, "hex");
    this.validateLength(buffer);
    return new BlockchainAddress(buffer);
  }

  /**
   * Creates an address from bytes.
   *
   * @param val the encoded address.
   * @return the blockchain address.
   */
  public static fromBuffer(val: Buffer): BlockchainAddress {
    const buffer = Buffer.from(val);
    this.validateLength(buffer);
    return new BlockchainAddress(buffer);
  }

  private static validateLength(val: Buffer) {
    if (val.length !== BlockchainAddress.ADDRESS_LENGTH) {
      throw new Error(
        `BlockchainAddress expects exactly ${BlockchainAddress.ADDRESS_LENGTH} bytes, but found ${val.length}`
      );
    }
  }
}
