/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { FiniteFieldElement } from "./FiniteFieldElement";

/** Polynomials with coefficients in a finite field. */
export class Polynomial<T extends FiniteFieldElement<T>> {
  private readonly coefficients: T[];

  private constructor(coefficients: T[]) {
    this.coefficients = coefficients;
  }

  /**
   * Construct a polynomial from a set of coefficients. The degree of the resultant polynomial is
   * assumed to be equal to the number of coefficients minus 1. The constant term of the polynomial
   * is assumed to be stored in the first position of the coefficients.
   *
   * @param coefficients the coefficients of the polynomial
   * @param zero zero element of the field
   * @return the constructed polynomial
   */
  static create<T extends FiniteFieldElement<T>>(coefficients: T[], zero: T): Polynomial<T> {
    return new Polynomial<T>(this.filterHighZeroes(coefficients, zero));
  }

  private static filterHighZeroes<T extends FiniteFieldElement<T>>(
    coefficients: T[],
    zero: T
  ): T[] {
    for (let i = coefficients.length - 1; i >= 0; i--) {
      if (!coefficients[i].isZero()) {
        return coefficients.slice(0, i + 1);
      }
    }
    return [zero];
  }

  /**
   * Returns the coefficients of the polynomial.
   *
   * @return the coefficients of the polynomial
   */
  getCoefficients(): T[] {
    return this.coefficients.slice();
  }

  /**
   * Returns the degree of this polynomial.
   *
   * @return the degree of the polynomial
   */
  degree(): number {
    return this.coefficients.length - 1;
  }

  /**
   * Returns the constant term of the polynomial.
   *
   * @return the term stored on position 0 of the coefficients of this polynomial
   */
  getConstantTerm(): T {
    return this.coefficients[0];
  }

  /**
   * Evaluates this polynomial on a point. That is, if F is the polynomial and x a value, then this
   * method returns F(x).
   *
   * @param point the point to evaluate this polynomial on
   * @return F(point) where F is this polynomial
   */
  evaluate(point: T): T {
    const degree = this.degree();

    let result = this.coefficients[degree];
    for (let i = degree - 1; i >= 0; --i) {
      const current = this.coefficients[i];
      result = current.add(point.multiply(result));
    }
    return result;
  }
}
