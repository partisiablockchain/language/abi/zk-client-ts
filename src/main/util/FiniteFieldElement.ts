/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/** A finite field element. */
export interface FiniteFieldElement<T> {
  /**
   * Field element addition.
   * @param other the right-hand side
   */
  readonly add: (other: T) => T;
  /**
   * Field element subtraction.
   * @param other the right-hand side
   */
  readonly subtract: (other: T) => T;
  /**
   * Field element multiplication.
   * @param other the right-hand side
   */
  readonly multiply: (other: T) => T;
  /**
   * Negate this element.
   */
  readonly negate: () => T;
  /**
   * Return the multiplicative inverse of this element.
   */
  readonly modInverse: () => T;
  /**
   * Get the square root of the element.
   */
  readonly squareRoot: () => T;
  /**
   * Whether this element is zero.
   * @returns true if zero
   */
  readonly isZero: () => boolean;
  /**
   * Whether this element is one.
   * @returns true if one
   */
  readonly isOne: () => boolean;
  /**
   * Whether this element is equal to the specified element.
   * @param other the element to check
   * @returns the equality
   */
  readonly isEqualTo: (other: T) => boolean;
}
