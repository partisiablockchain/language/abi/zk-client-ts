/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Encrypted secret shares of binary data. The secret shares have been encrypted for each of the
 * parties (engines) that will receive them. The order of the encrypted shares match the order of
 * the engines.
 */
export class EncryptedShares {
  readonly shares: EncryptedShare[];

  /**
   * Constructs encrypted shares from a list of shares. The list contains an encrypted share for
   * each receiving party of the secret input.
   *
   * @param shares the encrypted shares
   */
  constructor(shares: EncryptedShare[]) {
    this.shares = shares;
  }
}

export type EncryptedShare = Buffer;
